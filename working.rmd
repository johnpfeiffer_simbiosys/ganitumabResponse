---
title: "R Notebook"
output: html_notebook
---

```{r}
library(gdata)
```

```{r}

prePRO <- function(df,numerics,factors) {
  
  for (x in 1:ncol(df)) {

    if (factors==TRUE) {
      if (is.factor(df[,x])) {
        if (length(table(df[,x]))<3) {
          levels(df[,x]) <- c(levels(df[,x]), "missing")
          df[which(df[,x]==""),x]<-"missing"
          df[which(is.na(df[,x])),x]<-"missing"
          df[,x]<-factor(df[,x])
      } else {
          levels(df[,x]) <- c(levels(df[,x]), "missing")
          df[which(df[,x]==""),x]<-"missing"
          df[which(is.na(df[,x])),x]<-"missing"
          df[,x]<-factor(df[,x])
        }
      }
    }

    if (numerics==TRUE) {
      if (is.numeric(df[,x])) {
        obsNA<-length(which(is.na(df[,x])==TRUE))
        obsTotal<-nrow(df)
        percNA<-obsNA/obsTotal
        if (percNA>0.333) {
          df[,x]<-NA
        }
        if (percNA<0.333) {
          mu<-mean(df[,x], na.rm=TRUE)
          df[which(is.na(df[,x])),x]<-mu
          # print(paste0(colnames(df[,(x-1):x])[[2]],": ",mu))
        }
      }
    }
  }
  return(df)
}

removeNA <- function(df) {
  remove=NULL
  for (x in 1:ncol(all)) {
    if (length(which(is.na(all)))==nrow(all)) {
      remove<-c(remove,x)
    }
  if (x == ncol(all)) {
    if (is.null(remove)) {
      print("removed nothing")
    } else {
        all<-all[,-c(remove)]
      }
    }
  }
  return(df)
}

```

```{r}
radio_new = read.csv("/home/johnpfeiffer/Venv/ganitumabResponse/ISPY2_NewGanitumab/BioMarkers_NewGanitumab/extracted/radiomics.csv")
radio_new = radio_new[!duplicated(radio_new),]

growth_new = read.csv("/home/johnpfeiffer/Venv/ganitumabResponse/ISPY2_NewGanitumab/BioMarkers_NewGanitumab/extracted/growth_rates.csv")
growth_new = growth_new[!duplicated(growth_new),]

mvs_new = read.csv("/home/johnpfeiffer/Venv/ganitumabResponse/ISPY2_NewGanitumab/BioMarkers_NewGanitumab/extracted/mvs.csv")
mvs_new = mvs_new[!duplicated(mvs_new),]

nutrients_new = read.csv("/home/johnpfeiffer/Venv/ganitumabResponse/ISPY2_NewGanitumab/BioMarkers_NewGanitumab/extracted/nutrients.csv")
nutrients_new = nutrients_new[!duplicated(nutrients_new),]

densities_new = read.csv("/home/johnpfeiffer/Venv/ganitumabResponse/ISPY2_NewGanitumab/BioMarkers_NewGanitumab/extracted/shell_densities.csv")
densities_new = densities_new[!duplicated(densities_new),]

morph_new = read.csv("/home/johnpfeiffer/Venv/ganitumabResponse/ISPY2_NewGanitumab/BioMarkers_NewGanitumab/extracted/tumor_shapes.csv")
morph_new = morph_new[!duplicated(morph_new),]

new = data.frame(growth_new, mvs_new, nutrients_new, radio_new, densities_new, morph_new)
new$group = "new"
```

```{r}
radio_old = read.csv("/home/johnpfeiffer/Venv/ganitumabResponse/ISPY2_OldGanitumab/BioMarkers_OldGanitumab/extracted/radiomics.csv")
radio_old = radio_old[!duplicated(radio_old),]

growth_old = read.csv("/home/johnpfeiffer/Venv/ganitumabResponse/ISPY2_OldGanitumab/BioMarkers_OldGanitumab/extracted/growth_rates.csv")
growth_old = growth_old[!duplicated(growth_old),]

mvs_old = read.csv("/home/johnpfeiffer/Venv/ganitumabResponse/ISPY2_OldGanitumab/BioMarkers_OldGanitumab/extracted/mvs.csv")
mvs_old = mvs_old[!duplicated(mvs_old),]

nutrients_old = read.csv("/home/johnpfeiffer/Venv/ganitumabResponse/ISPY2_OldGanitumab/BioMarkers_OldGanitumab/extracted/nutrients.csv")
nutrients_old = nutrients_old[!duplicated(nutrients_old),]

densities_old = read.csv("/home/johnpfeiffer/Venv/ganitumabResponse/ISPY2_OldGanitumab/BioMarkers_OldGanitumab/extracted/shell_densities.csv")
densities_old = densities_old[!duplicated(densities_old),]

morph_old = read.csv("/home/johnpfeiffer/Venv/ganitumabResponse/ISPY2_OldGanitumab/BioMarkers_OldGanitumab/extracted/tumor_shapes.csv")
morph_old = morph_old[!duplicated(morph_old),]


radio_old = radio_old[radio_old$caseID %in% growth_old$caseID,]
mvs_old = mvs_old[mvs_old$caseID %in% growth_old$caseID,]
nutrients_old = nutrients_old[nutrients_old$caseID %in% growth_old$caseID,]
densities_old = densities_old[densities_old$caseID %in% growth_old$caseID,]
morph_old = morph_old[morph_old$caseID %in% growth_old$caseID,]

old = data.frame(growth_old, mvs_old, nutrients_old, radio_old, densities_old, morph_old)
old$group = "old"
```

```{r}
sim = rbind(new, old)
rownames(sim) = sim$caseID
sim = sim[,3:ncol(sim)]
remove01 = grep("1",colnames(sim))
remove02 = grep("2",colnames(sim))
sim = sim[,-c(remove01, remove02)]
```

```{r}
keep(sim, prePRO, removeNA, sure=TRUE)
```

```{r}
clin_old = read.csv("/home/johnpfeiffer/Venv/ganitumabResponse/ISPY2_OldGanitumab/SBS_OldGanitumab_DataEntry_07.27.22_JP.csv")
clin_new = read.csv("/home/johnpfeiffer/Venv/ganitumabResponse/ISPY2_NewGanitumab/SBS_NewGanitumab_DataEntry_08.02.22_JP.csv")

clin = rbind(clin_old, clin_new)
clin = clin[clin$caseID %in% rownames(sim),]
rownames(clin) = clin$caseID

outcome = clin[,c("caseID","pathCompleteResponse")]

clin = clin[,c("age","raceEthnicity", "fdaHR", "mammaPrintIndex")]
```

```{r}
sim = sim[rownames(sim) %in% rownames(clin),]
```

```{r}
all = merge(clin,sim,by=0)
keep(all, outcome, prePRO, removeNA, sure=TRUE)
```

```{r adding NAs and ensuring factor}
for (idx in 1:ncol(all)) {
  if (class(all[,idx])=="character") {
    all[,idx]<-factor(all[,idx])
  }
}; rm(idx) 
```

```{r}
for (x in 1:ncol(all)) {
  if (is.numeric(all[,x])) { # if its numeric
    if (sd(all[,x])!=0) { # if the standard deviation is > 0
      sk<-skewness(all[,x])
      if (sk > 1.5) {  # if skewness > 1.5
        if (min(all[,x]) > 0) {  # if min is positive
          all[,x]<-log(all[,x])  # take log
        } else {  # if min is negative
            subtracting = min(all[,x]) # shift dist to min 0
            all[,x]<-log(all[,x]-subtracting)  # take log
        }
      }
    }
  }
  if (is.integer(all[,x])) { # repeat above for integer class
    if (sd(all[,x])!=0) {
      sk<-skewness(all[,x])
      if (sk > 1.5) {
        if (min(all[,x]) > 0) {
          all[,x]<-log(all[,x])
        } else {
            subtracting = min(all[,x])
            all[,x]<-log(all[,x]-subtracting)
        }
      }
    }
  }
}; rm(sk,x)
```

```{r}
all = prePRO(all, factors = TRUE, numerics = TRUE)
all = removeNA(all)
complete.cases(all)
```

```{r}
keep(all,outcome, sure=TRUE)
gc(verbose=TRUE, full=TRUE)
```

```{r creating data object for modeling}
all = all[order(all$Row.names),]
outcome = outcome[order(outcome$caseID),]

model_data<-all[which(complete.cases(all)),]
all(rownames(outcome)==model_data$Row.names)
model_data$pathCompleteResponse<-factor(outcome$pathCompleteResponse)
rownames(model_data) = model_data$Row.names
model_data = model_data[,2:ncol(model_data)]
```



```{r}
### only clinical model
control <- trainControl(method="LOOCV", savePredictions = "all", sampling = "smote", search = "grid")
metric="Kappa"
clinical_fit<- train(form = pathCompleteResponse ~ fdaHR  + age + raceEthnicity,
               data=model_data, method="glm", metric="Kappa", maximize = TRUE, trControl=control); clinical_fit
clinical_obj<-clinical_fit$pred[,1:2]; clinical_obj
clinical_output <- confusionMatrix(data=clinical_obj$pred, reference = clinical_obj$obs, positive = "TRUE"); clinical_output
clinical_modelsummary<-summary(glm(pathCompleteResponse ~ fdaHR  + age + raceEthnicity, data=model_data, family="binomial")); clinical_modelsummary
```

```{r}
control <- trainControl(method="LOOCV", savePredictions = "all", sampling = "smote", search = "grid")
metric="Kappa"
clinical_combatSim_fit <- train(form = pathCompleteResponse ~ fdaHR  + age + raceEthnicity + radiomic_Contrast_GLSZM_SmallAreaEmphasis + Kt_cancer_median_at_mri,
               data=model_data, method="glm", metric="Kappa", maximize = TRUE, trControl=control); clinical_combatSim_fit
clinical_combatSim_obj<-clinical_combatSim_fit$pred[,1:2]; clinical_combatSim_obj
clinical_combatSim_output <- confusionMatrix(data=clinical_combatSim_obj$pred, reference = clinical_combatSim_obj$obs, positive = "TRUE"); clinical_combatSim_output
clinical_combatSim_modelsummary<-summary(glm(pathCompleteResponse ~ fdaHR  + age + raceEthnicity + radiomic_Contrast_GLSZM_SmallAreaEmphasis + Kt_cancer_median_at_mri,
                   data=model_data, family="binomial")); clinical_combatSim_modelsummary
```







