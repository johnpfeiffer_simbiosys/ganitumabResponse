



feature_df = data.frame(all)

fac_terms = c("pathCompleteResponse","fdaHR","grade","treatment", "raceEthnicity")
num_terms = c("median_Kt_regimen_wk1","pre_tumor_glszm_smallareaemphasis","SEC11A")

num_values<-feature_df[,num_terms]
for (x in 1:ncol(num_values)) {
  num_values[,x]<-scale(num_values[,x])}

start_idx = 5
end_idx = ncol(all)

control <- trainControl(method="LOOCV", savePredictions = "all", sampling = "smote", search = "grid", classProbs = TRUE)
metric="Kappa"

result_df_00=data.frame()


model_data<-data.frame(feature_df[,fac_terms], num_values)
model_data<-model_data[which(complete.cases(model_data)),]
model_data$pathCompleteResponse<-factor(model_data$pathCompleteResponse)
model_data$grade<-factor(model_data$grade)
model_data$fdaHR<-factor(model_data$fdaHR)
model_data$treatment<-factor(model_data$treatment)
model_data$raceEthnicity<-factor(model_data$raceEthnicity)
  
fit <- train(form = pathCompleteResponse ~ fdaHR + grade + raceEthnicity + median_Kt_regimen_wk1 + pre_tumor_glszm_smallareaemphasis + treatment * SEC11A, 
               data=model_data, method="glm", metric="Kappa", maximize = TRUE, trControl=control); fit

predTable<-fit[["pred"]]

aucVal<-auc(roc(predTable, obs, pCR))

result_df_00[1,1]<-aucVal




##############################################################################################################################################################################

feature_df = data.frame(all)

fac_terms = c("pathCompleteResponse","fdaHR","grade","treatment", "raceEthnicity")
num_terms = c("median_Kt_regimen_wk1","pre_tumor_glszm_smallareaemphasis")

result_df_totalFeatures_full<-result_df_totalFeatures_full[order(result_df_totalFeatures_full$Kappa, decreasing = T),]
interest<-rownames(result_df_totalFeatures_full[1:1000,])

num_values<-feature_df[,num_terms]
for (x in 1:ncol(num_values)) {
  num_values[,x]<-scale(num_values[,x])}

control <- trainControl(method="LOOCV", savePredictions = "all", sampling = "smote", search = "grid", classProbs = TRUE)
metric="Kappa"

####################################################################################################################


for (x in interest) {
  feature<-feature_df[,x]
  if (!is.numeric(feature)) {
    feature<-factor(feature)  
  } else {
    feature<-scale(feature)
  }
  model_data<-data.frame(feature_df[,fac_terms], num_values)
  model_data<-cbind(model_data,feature)
  model_data<-model_data[which(complete.cases(model_data)),]
  model_data$pathCompleteResponse<-factor(model_data$pathCompleteResponse)
  model_data$grade<-factor(model_data$grade)
  model_data$fdaHR<-factor(model_data$fdaHR)
  model_data$treatment<-factor(model_data$treatment)
  model_data$raceEthnicity<-factor(model_data$raceEthnicity)
  
  fit <- train(form = pathCompleteResponse ~ fdaHR + grade + raceEthnicity + median_Kt_regimen_wk1 + pre_tumor_glszm_smallareaemphasis + treatment * feature, 
               data=model_data, method="glm", metric="Kappa", maximize = TRUE, trControl=control); fit
  
  predTable<-fit[["pred"]]
  aucVal<-auc(roc(predTable, obs, pCR))
  result_df_totalFeatures_full_auc[x,"AUC"]<-aucVal

  }
result_df_totalFeatures_full_auc<-result_df_totalFeatures_full; rm(result_df_totalFeatures_full)

View(result_df_totalFeatures_full_auc)

### clean up the env
gdata::keep(final,all,clinical_fit, clinical_output, clinical_obj, clinical_modelsummary, 
            clinical_combatSim_fit, clinical_combatSim_output, clinical_combatSim_obj, clinical_combatSim_modelsummary, 
            result_df_totalFeatures_full_auc,sure=TRUE)
gc(verbose=TRUE, full=TRUE)


write.csv(result_df_totalFeatures_full_auc,"/home/johnpfeiffer/Desktop/gmrv_final/SBS_GMR_InteractionModel_GLM_resultDF_fdaHR_grade_race_medKT_smallAE_AUC_02.18.22_JP.csv")
save.image("/home/johnpfeiffer/Desktop/gmrv_final/rdata/SBS_GMR_InteractionModel_GLM_resultDF_fdaHR_grade_race_medKT_smallAE_AUC_02.18.22_JP.RData")








####################################################################################################################################################################


theirs<-read.csv("/home/johnpfeiffer/Desktop/gmrv_final/GSE180962_ISPY2ResID_Ganitumab_biomarkers_20210607.csv",row.names = 2)
theirs<-theirs[,2:ncol(theirs)]
View(theirs)

all<-merge(all,theirs,by=0)

feature_df = data.frame(all)

fac_terms = c("pathCompleteResponse","fdaHR","grade","treatment", "raceEthnicity")
num_terms = c("median_Kt_regimen_wk1","pre_tumor_glszm_smallareaemphasis")

num_values<-feature_df[,num_terms]
for (x in 1:ncol(num_values)) {
  num_values[,x]<-scale(num_values[,x])}

start_idx = 21521
end_idx = ncol(all)

control <- trainControl(method="LOOCV", savePredictions = "all", sampling = "smote", search = "grid", classProbs = TRUE)
metric="Kappa"

result_df_00=data.frame()


##########################################################


for (x in start_idx:end_idx) {
  feature<-feature_df[,x]
  if (!is.numeric(feature)) {
    feature<-factor(feature)  
  } else {
    feature<-scale(feature)
  }
  model_data<-data.frame(feature_df[,fac_terms], num_values)
  model_data<-cbind(model_data,feature)
  model_data<-model_data[which(complete.cases(model_data)),]
  model_data$pathCompleteResponse<-factor(model_data$pathCompleteResponse)
  model_data$grade<-factor(model_data$grade)
  model_data$fdaHR<-factor(model_data$fdaHR)
  model_data$treatment<-factor(model_data$treatment)
  model_data$raceEthnicity<-factor(model_data$raceEthnicity)
  
  fit <- train(form = pathCompleteResponse ~ fdaHR + grade + raceEthnicity + median_Kt_regimen_wk1 + pre_tumor_glszm_smallareaemphasis + treatment * feature, 
               data=model_data, method="glm", metric="Kappa", maximize = TRUE, trControl=control); fit
  
  obj<-fit$pred[,1:2]
  out <- confusionMatrix(data=obj$pred, reference = obj$obs, positive = "pCR")
  result_df_00[x,1:7]<-data.frame(t(out$overall))
  result_df_00[x,8:18]<-data.frame(t(out$byClass))
  
  predTable<-fit[["pred"]]
  aucVal<-auc(roc(predTable, obs, pCR))
  result_df_00[x,"AUC"]<-aucVal
  
  result_df_00[x,19]
  print(x)
}

result_df2<-result_df_00[start_idx:(x-1), ]
result_df_t<-as.data.frame(t(result_df2))
idx<-as.numeric(colnames(result_df_t))
colnames(result_df_t)<-colnames(feature_df[,idx])
result_df2<-data.frame(t(result_df_t)); rm(idx, result_df_t)
ispy_bioM<-result_df2
View(ispy_bioM)

### clean up the env
gdata::keep(final,all,clinical_fit, clinical_output, clinical_obj, clinical_modelsummary, 
            clinical_combatSim_fit, clinical_combatSim_output, clinical_combatSim_obj, clinical_combatSim_modelsummary, ispy_bioM,sure=TRUE)
gc(verbose=TRUE, full=TRUE)


write.csv(ispy_bioM,"/home/johnpfeiffer/Desktop/gmrv_final/SBS_GMR_InteractionModel_GLM_resultDF_fdaHR_grade_race_medKT_smallAE_ISPY2bioM_02.18.22_JP.csv")
save.image("/home/johnpfeiffer/Desktop/gmrv_final/rdata/SBS_GMR_InteractionModel_GLM_resultDF_fdaHR_grade_race_medKT_smallAE_ISPY2bioM_02.18.22_JP.RData")

